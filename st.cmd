require essioc
require mrfioc2, 2.3.1+15
require sdsioc, 0.0.1+1

#-All systems will have counters for 14Hz, PMorten and DoD
epicsEnvSet("EVREVTARGS" "N0=F14Hz,E0=14,N1=PMortem,E1=40,N2=DoD,E2=42")
epicsEnvSet("EVREVTARGS" "$(EVREVTARGS),N3=LEBTCSt,E3=6,N4=LEBTCEnd,E4=7,N5=MEBTCSt,E5=8,N6=MEBTCEnd,E6=9,N7=IonMagSt,E7=10,N8=IonMagEnd,E8=11,N9=BPulseSt,E9=12,NA=BPulseEnd,EA=13,NB=RFSt,EB=15,NC=PMortemSys,EC=41,ND=DoDSys,ED=43")

iocshLoad "$(mrfioc2_DIR)/evr.iocsh"      "P=FBIS-DLN02:Ctrl-EVR-01,PCIID=0e:00.0,EVRDB=$(EVRDB=evr-mtca-300-univ.db)"
dbLoadRecords "evr-databuffer-ess.db"     "P=FBIS-DLN02:Ctrl-EVR-01"
iocshLoad "$(mrfioc2_DIR)/evrevt.iocsh"   "P=FBIS-DLN02:Ctrl-EVR-01,$(EVREVTARGS=)"

afterInit('iocshLoad($(mrfioc2_DIR)/evr.r.iocsh                   "P=FBIS-DLN02:Ctrl-EVR-01, INTREF=#")')
afterInit('iocshLoad($(mrfioc2_DIR)/evrtclk.r.iocsh               "P=FBIS-DLN02:Ctrl-EVR-01")')




#-Load all default values for FBIS EVR
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:BPIn-2-Code-Back-SP, VAL=41"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:BPIn-2-Code-Ext-SP, VAL=41"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:BPIn-2-Trig-Back-Sel, VAL=2"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:BPIn-2-Trig-Ext-Sel, VAL=2"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:BPIn-3-Code-Back-SP, VAL=41"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:BPIn-3-Code-Ext-SP, VAL=41"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:BPIn-3-Trig-Back-Sel, VAL=2"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:BPIn-3-Trig-Ext-Sel, VAL=2"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:BPIn-4-Code-Ext-SP, VAL=0"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:BPIn-4-Trig-Ext-Sel, VAL=0"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:BPIn-6-Code-Back-SP, VAL=41"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:BPIn-6-Code-Ext-SP, VAL=41"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:BPIn-6-Trig-Back-Sel, VAL=2"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:BPIn-6-Trig-Ext-Sel, VAL=2"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:BPIn-7-Code-Back-SP, VAL=41"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:BPIn-7-Code-Ext-SP, VAL=41"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:BPIn-7-Trig-Back-Sel, VAL=2"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:BPIn-7-Trig-Ext-Sel, VAL=2"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-0-Evt-Trig0-SP, VAL=14"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-0-Width-SP, VAL=1000"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-10-Evt-Trig0-SP, VAL=7"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-10-Width-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-11-Evt-Trig0-SP, VAL=6"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-11-Width-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-12-Evt-Trig0-SP, VAL=8"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-12-Evt-Trig1-SP, VAL=9"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-12-Width-SP, VAL=100"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-1-Evt-Trig0-SP, VAL=15"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-1-Width-SP, VAL=1000"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-2-Evt-Trig0-SP, VAL=12"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-2-Width-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-3-Evt-Trig0-SP, VAL=13"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-3-Width-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-4-Evt-Trig0-SP, VAL=125"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-4-Width-SP, VAL=100"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-5-Evt-Trig0-SP, VAL=40"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-5-Width-SP, VAL=100"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-7-Evt-Trig0-SP, VAL=42"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-7-Width-SP, VAL=100"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-8-Evt-Trig0-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-8-Width-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-9-Evt-Trig0-SP, VAL=11"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:DlyGen-9-Width-SP, VAL=10"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-Back0-Src-Pulse-SP, VAL=0"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-Back1-Src-Scale-SP, VAL=11"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-Back2-Src-Scale-SP, VAL=11"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-Back3-Src-Scale-SP, VAL=11"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-Back4-Src-SP, VAL=5"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-Back5-Src-Scale-SP, VAL=11"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-Back6-Src-Scale-SP, VAL=11"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-Back7-Src-Scale-SP, VAL=11"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-FP0-Src-SP, VAL=53"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-FP1-Src-SP, VAL=15"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-FP2-Src-SP, VAL=15"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-FP3-Src-SP, VAL=15"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-FPUV0-Src-SP, VAL=52"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-FPUV1-Src-SP, VAL=15"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-FPUV2-Src-SP, VAL=15"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-FPUV3-Src-SP, VAL=15"
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-Int-Src-SP, VAL=0"
dbLoadRecords "initial-string-waveform.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-Back0-Label-I, VAL=EvtF14Hz"
dbLoadRecords "initial-string-waveform.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-Back1-Label-I, VAL=Beam Inhibit A"
dbLoadRecords "initial-string-waveform.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-Back2-Label-I, VAL=PMortemSys (Regular Beam Interlock A)"
dbLoadRecords "initial-string-waveform.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-Back3-Label-I, VAL=PMortemSys (Emergency Beam Interlock A)"
dbLoadRecords "initial-string-waveform.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-Back4-Label-I, VAL=PMortem"
dbLoadRecords "initial-string-waveform.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-Back5-Label-I, VAL=Beam Inhibit B"
dbLoadRecords "initial-string-waveform.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-Back6-Label-I, VAL=PMortemSys (Regular Beam Interlock B)"
dbLoadRecords "initial-string-waveform.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:Out-Back7-Label-I, VAL=PMortemSys (Emergency Beam Interlock B)"

#- Load Beam Inhibt from databuffer for FBIS
dbLoadRecords "beaminhibt-fbis.template" "P=FBIS-DLN02:Ctrl-EVR-01:,INPARR=dbus-recv-u32"

#- Connect Backplane 5 to bit 7 of Dbus // This allows FBIS control Beam Inhibt
dbLoadRecords "initial-value.template"     "PVNAME=FBIS-DLN02:Ctrl-EVR-01:BPIn-5-DBus-Sel, VAL=7"








#- Fixing IdCycle to 64bits
dbLoadRecords "cycleid-64bits.template"               "P=FBIS-DLN02:Ctrl-EVR-01:"
#- Fixing databuffer Information
dbLoadRecords "BDest.template" "P=FBIS-DLN02:Ctrl-EVR-01:,PV=BDest-I"
dbLoadRecords "BMod.template" "P=FBIS-DLN02:Ctrl-EVR-01:,PV=BMod-I"
dbLoadRecords "BPresent.template" "P=FBIS-DLN02:Ctrl-EVR-01:,PV=BPresent-I"
#- Configuring PVs to be archived
iocshLoad("archive-default.iocsh","P=FBIS-DLN02:Ctrl-EVR-01:")


#- ----------------------------------------------------------------------------
#- SDS Metadata Capture
#- ----------------------------------------------------------------------------
iocshLoad("$(sdsioc_DIR)/sdsCreateMetadataEVR.iocsh","PEVR=FBIS-DLN02:Ctrl-EVR-01:,F14Hz=F14Hz")

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

